def call() {
    withCredentials([usernamePassword(credentialsId: 'jira-nims', passwordVariable: 'pass', usernameVariable: 'usr')]) {
    sh 'curl -D- -u ${usr}:${pass} -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d "{ \\"fields\\": { \\"project\\": { \\"key\\": \\"EDD\\" },\\"summary\\": \\"Build ${BUILD_NUMBER} failed\\", \\"issuetype\\": { \\"name\\": \\"Task\\" }}}" https://nimishajira.atlassian.net/rest/api/3/issue'
}
}